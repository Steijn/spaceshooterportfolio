﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    public Transform target;

	void Update ()
    {
        transform.position = target.position + Vector3.back * 10f;
	}
}
