﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level : MonoBehaviour
{
    [Header("Scripts Enemys")]
    [SerializeField]
    public AiSpawner m_Enemy1;
    [SerializeField]
    public AiSpawner m_Enemy2;
    [SerializeField]
    public AiSpawner m_Enemy3;
    [SerializeField]
    public AiSpawner m_Enemy4;

    [Header("Scripts Rocks")]
    [SerializeField]
    public AiSpawner m_Rock1;
    [SerializeField]
    public AiSpawner m_Rock2;
    [SerializeField]
    public AiSpawner m_Rock3;
    [SerializeField]
    public AiSpawner m_Rock4;

    [Header("Script")]
    [SerializeField]
    public float m_MaxTimer;
    public float m_CurrenTimer;

    [SerializeField]
    private int m_Enemys;
    [SerializeField]
    private float m_ESpawnRate;
    [SerializeField]
    private int m_Rocks;
    [SerializeField]
    private float m_RSpawnRate;

    [SerializeField]
    public Text m_LText;
    public float m_LCounter = 1;

    [SerializeField]
    public float m_MaxLevels;
    public bool m_Allowed = true;

    private int saveAantalEnemy;
    private float saveESpawnrate;
    private int saveAantalRock;
    private float saveRSpawnrate;

    void Start()
    {
        saveAantalEnemy = m_Enemy1.m_AantalAI;
        saveESpawnrate = m_Enemy1.m_SpawnRate;
        saveAantalRock = m_Rock1.m_AantalAI;
        saveRSpawnrate = m_Rock1.m_SpawnRate;

        m_CurrenTimer = m_MaxTimer;

        m_LText.text = "Level " + m_LCounter;
    }

    void Update()
    {
        if (m_Allowed == true)
        {
            m_CurrenTimer -= Time.deltaTime;
        }

        if (m_CurrenTimer <= 0)
        {
            m_Enemy1.m_AantalAI += m_Enemys;
            m_Enemy2.m_AantalAI += m_Enemys;
            m_Enemy3.m_AantalAI += m_Enemys;
            m_Enemy4.m_AantalAI += m_Enemys;

            m_Enemy1.m_SpawnRate -= m_ESpawnRate;
            m_Enemy2.m_SpawnRate -= m_ESpawnRate;
            m_Enemy3.m_SpawnRate -= m_ESpawnRate;
            m_Enemy4.m_SpawnRate -= m_ESpawnRate;

            m_Rock1.m_AantalAI += m_Rocks;
            m_Rock2.m_AantalAI += m_Rocks;
            m_Rock3.m_AantalAI += m_Rocks;
            m_Rock4.m_AantalAI += m_Rocks;

            m_Rock1.m_SpawnRate -= m_RSpawnRate;
            m_Rock2.m_SpawnRate -= m_RSpawnRate;
            m_Rock3.m_SpawnRate -= m_RSpawnRate;
            m_Rock4.m_SpawnRate -= m_RSpawnRate;

            m_CurrenTimer = m_MaxTimer;
            m_LCounter += 1;

            m_LText.text = "Level " + m_LCounter;
        }

        if (m_LCounter == m_MaxLevels)
        {
            m_Allowed = false;
        }
    }

    public void Reset()
    {
        m_Enemy1.m_AantalAI = saveAantalEnemy;
        m_Enemy2.m_AantalAI = saveAantalEnemy;
        m_Enemy3.m_AantalAI = saveAantalEnemy;
        m_Enemy4.m_AantalAI = saveAantalEnemy;

        m_Enemy1.m_SpawnRate = saveESpawnrate;
        m_Enemy2.m_SpawnRate = saveESpawnrate;
        m_Enemy3.m_SpawnRate = saveESpawnrate;
        m_Enemy4.m_SpawnRate = saveESpawnrate;

        m_Rock1.m_AantalAI = saveAantalRock;
        m_Rock2.m_AantalAI = saveAantalRock;
        m_Rock3.m_AantalAI = saveAantalRock;
        m_Rock4.m_AantalAI = saveAantalRock;

        m_Rock1.m_SpawnRate = saveRSpawnrate;
        m_Rock2.m_SpawnRate = saveRSpawnrate;
        m_Rock3.m_SpawnRate = saveRSpawnrate;
        m_Rock4.m_SpawnRate = saveRSpawnrate;

        m_LCounter = 0;
        m_LText.text = "Level " + m_LCounter;

        m_Allowed = false;
    }
}
