﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiHealth : MonoBehaviour
{
    [SerializeField]
    HealthController m_Health;
    [SerializeField]
    ScoreManager m_Score;
    [SerializeField]
    private int m_Points;

    [SerializeField]
    private float m_MaxHealth;
    private float m_CurHealth;

    private float m_SaveMax;

    void Awake()
    {
        m_CurHealth = m_SaveMax;
    }

    void Start()
    {
        m_SaveMax = m_MaxHealth;

        m_Score = FindObjectOfType<ScoreManager>();
        m_Health = FindObjectOfType<HealthController>();

        m_CurHealth = m_MaxHealth;
    }

    public void DecreaseHealths(float Damage)
    {
        m_CurHealth -= Damage;
        ZeroHealth();
    }

    void ZeroHealth()
    {
        if (m_CurHealth <= 0)
        {
            m_Score.m_Number += m_Points;
            gameObject.SetActive(false);
        }
    }

    private void PlayerDied()
    {
        if(m_Health.m_CurHealth <= 0)
        {
            gameObject.SetActive(false);
            m_SaveMax = m_MaxHealth;
        }
    }

    private void Update()
    {
        PlayerDied();
    }
}
