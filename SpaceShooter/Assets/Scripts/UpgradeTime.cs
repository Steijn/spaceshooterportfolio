﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeTime : MonoBehaviour
{
    [Header("Scripts Needed")]
    [SerializeField]
    PlayerController m_Player;
    [SerializeField]
    HealthController m_Health;
    [SerializeField]
    ScoreManager m_Score;
    [SerializeField]
    BulletDamage m_Bullet;
    [SerializeField]
    Level m_Level;
    [SerializeField]
    ChooseDif m_Dif;

    [Header("Upgrade")]
    [SerializeField]
    private GameObject m_UpgradeMenu;

    [Header("Spawners Right")]
    [SerializeField]
    private GameObject m_AISpawner1;
    [SerializeField]
    private GameObject m_RockSpawner1;

    [Header("Spawners Up")]
    [SerializeField]
    private GameObject m_AISpawner2;
    [SerializeField]
    private GameObject m_RockSpawner2;

    [Header("Spawners Down")]
    [SerializeField]
    private GameObject m_AISpawner3;
    [SerializeField]
    private GameObject m_RockSpawner3;

    [Header("Spawners Left")]
    [SerializeField]
    private GameObject m_AISpawner4;
    [SerializeField]
    private GameObject m_RockSpawner4;

    [Header("Upgrade Cost")]
    [SerializeField]
    private Text m_CostFireRate;
    private int m_Number3 = 300;

    [SerializeField]
    private Text m_CostHealth;
    private int m_Number1 = 200;

    [SerializeField]
    private Text m_CostDamage;
    private int m_Number2 = 100;

    [Header("Upgraded Level")]
    [SerializeField]
    private Text m_LevelFireRate;
    private int m_Number4;

    [SerializeField]
    private Text m_LevelHealth;
    private int m_Number5;

    [SerializeField]
    private Text m_LevelDamage;
    private int m_Number6;

    private float m_SpeedSave;

    private float m_UpgradedHealth;
    private float m_UpgradedDamage;
    private float m_UpgradedFireRate;

    private void Start()
    {
        m_SpeedSave = m_Player.speed;
        m_UpgradedFireRate = m_Player.ShootDelay;
        m_UpgradedDamage = m_Bullet.m_Damage;
        m_UpgradedHealth = m_Health.m_MaxHealth;
        m_Health.onHealthChanged += onHealthChanged;
    }

    private void onHealthChanged(float newHealth)
    {
        ZeroHealth();
    }

    private void OnDestroy()
    {
        m_Health.onHealthChanged -= onHealthChanged;
    }

    private void ZeroHealth()
    {
        if(m_Health.m_CurHealth <= 0)
        {
            m_UpgradeMenu.SetActive(true);

            m_Player.speed = 0;

            m_AISpawner1.SetActive(false);
            m_RockSpawner1.SetActive(false);

            m_AISpawner2.SetActive(false);
            m_RockSpawner2.SetActive(false);

            m_AISpawner3.SetActive(false);
            m_RockSpawner3.SetActive(false);

            m_AISpawner4.SetActive(false);
            m_RockSpawner4.SetActive(false);

            m_Level.Reset();

            m_Dif.m_Text.text = "Level " + m_Level.m_LCounter;
        }
    }

    //Button Close Menu
    public void CloseMenu()
    {
        m_Health.ChangeHealth(m_UpgradedHealth);

        m_UpgradeMenu.SetActive(false);

        m_Player.speed = m_SpeedSave;

        m_AISpawner1.SetActive(true);
        m_RockSpawner1.SetActive(true);

        m_AISpawner2.SetActive(true);
        m_RockSpawner2.SetActive(true);

        m_AISpawner3.SetActive(true);
        m_RockSpawner3.SetActive(true);

        m_AISpawner4.SetActive(true);
        m_RockSpawner4.SetActive(true);

        m_Level.m_Allowed = true;
    }

    public void UpgradeFireRate()
    {
        if(m_Score.m_Number >= m_Number3)
        {
            m_Score.m_Number -= m_Number3;
            m_Number3 *= 2;
            m_Number4 += 1;
            m_CostFireRate.text = m_Number3.ToString();
            m_LevelFireRate.text = m_Number4.ToString();
            m_Player.ShootDelay -= 0.05f;
        }
    }
    public void UpgradeHealth()
    {
        if (m_Score.m_Number >= m_Number1)
        {
            m_Score.m_Number -= m_Number1;
            m_Number1 *= 2;
            m_Number5 += 1;
            m_CostHealth.text = m_Number1.ToString();
            m_LevelHealth.text = m_Number5.ToString();
            m_Health.m_MaxHealth += 20;
            m_UpgradedHealth = m_Health.m_MaxHealth;
            m_Health.ChangeHealth(0);
        }
    }
    public void UpgradeDamage()
    {
        if (m_Score.m_Number >= m_Number2)
        {
            m_Score.m_Number -= m_Number2;
            m_Number2 *= 2;
            m_Number6 += 1;
            m_CostDamage.text = m_Number2.ToString();
            m_LevelDamage.text = m_Number6.ToString();
            m_Bullet.m_Damage += 10;
        }
    }
}