﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChooseDif : MonoBehaviour
{
    [SerializeField]
    public Text m_Text;
    [SerializeField]
    Level m_Level;

    [SerializeField]
    private int m_Enemys;
    [SerializeField]
    private float m_ESpawnRate;
    [SerializeField]
    private int m_Rocks;
    [SerializeField]
    private float m_RSpawnRate;

    private bool m_MagPlus = true;
    private bool m_MagMin = true;

    public void Plus()
    {
        if(m_MagPlus == true)
        {
            m_Level.m_Enemy1.m_AantalAI += m_Enemys;
            m_Level.m_Enemy2.m_AantalAI += m_Enemys;
            m_Level.m_Enemy3.m_AantalAI += m_Enemys;
            m_Level.m_Enemy4.m_AantalAI += m_Enemys;

            m_Level.m_Enemy1.m_SpawnRate -= m_ESpawnRate;
            m_Level.m_Enemy2.m_SpawnRate -= m_ESpawnRate;
            m_Level.m_Enemy3.m_SpawnRate -= m_ESpawnRate;
            m_Level.m_Enemy4.m_SpawnRate -= m_ESpawnRate;

            m_Level.m_Rock1.m_AantalAI += m_Rocks;
            m_Level.m_Rock2.m_AantalAI += m_Rocks;
            m_Level.m_Rock3.m_AantalAI += m_Rocks;
            m_Level.m_Rock4.m_AantalAI += m_Rocks;

            m_Level.m_Rock1.m_SpawnRate -= m_RSpawnRate;
            m_Level.m_Rock2.m_SpawnRate -= m_RSpawnRate;
            m_Level.m_Rock3.m_SpawnRate -= m_RSpawnRate;
            m_Level.m_Rock4.m_SpawnRate -= m_RSpawnRate;

            m_Level.m_CurrenTimer = m_Level.m_MaxTimer;
            m_Level.m_LCounter += 1;

            m_Level.m_LText.text = "Level " + m_Level.m_LCounter;
            m_Text.text = "Level " + m_Level.m_LCounter;

            m_MagMin = true;

            if (m_Level.m_LCounter == 10)
            {
                m_MagPlus = false;
            }
        }
    }

    public void Min()
    {
        if (m_MagMin == true)
        {
            m_Level.m_Enemy1.m_AantalAI -= m_Enemys;
            m_Level.m_Enemy2.m_AantalAI -= m_Enemys;
            m_Level.m_Enemy3.m_AantalAI -= m_Enemys;
            m_Level.m_Enemy4.m_AantalAI -= m_Enemys;

            m_Level.m_Enemy1.m_SpawnRate += m_ESpawnRate;
            m_Level.m_Enemy2.m_SpawnRate += m_ESpawnRate;
            m_Level.m_Enemy3.m_SpawnRate += m_ESpawnRate;
            m_Level.m_Enemy4.m_SpawnRate += m_ESpawnRate;

            m_Level.m_Rock1.m_AantalAI -= m_Rocks;
            m_Level.m_Rock2.m_AantalAI -= m_Rocks;
            m_Level.m_Rock3.m_AantalAI -= m_Rocks;
            m_Level.m_Rock4.m_AantalAI -= m_Rocks;

            m_Level.m_Rock1.m_SpawnRate += m_RSpawnRate;
            m_Level.m_Rock2.m_SpawnRate += m_RSpawnRate;
            m_Level.m_Rock3.m_SpawnRate += m_RSpawnRate;
            m_Level.m_Rock4.m_SpawnRate += m_RSpawnRate;

            m_Level.m_CurrenTimer = m_Level.m_MaxTimer;
            m_Level.m_LCounter -= 1;

            m_Level.m_LText.text = "Level " + m_Level.m_LCounter;
            m_Text.text = "Level " + m_Level.m_LCounter;

            m_MagPlus = true;

            if (m_Level.m_LCounter == 0)
            {
                m_MagMin = false;
            }
        }
    }

    private void Update()
    {
        if (m_Level.m_LCounter == 0)
        {
            m_MagMin = false;
        }
    }
}
