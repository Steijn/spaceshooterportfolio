﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageScript : MonoBehaviour
{
    [SerializeField]
    private float m_Damage;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        HealthController healthcontroller = coll.GetComponent<HealthController>();

        if (healthcontroller != null)
        {
            healthcontroller.ChangeHealth(-m_Damage);
            gameObject.SetActive(false);
        }
    }
}
