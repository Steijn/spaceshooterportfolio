﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //code for player
    public float speed;

    private float moveX, moveY, lookX, lookY;

    private Rigidbody2D rb;

    //Code for bullet
    [SerializeField] private float bulletSpeed;
    [SerializeField] private Rigidbody bullet;

    private Vector3 playerPos;
    private Vector3 playerDirection;
    private Quaternion playerRotation;

    [SerializeField]
    private float spawnDistance = 2;

    private float Timer;
    public float ShootDelay = 0.30f;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        moveX = Input.GetAxis("Horizontal");
        moveY = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveX, moveY, 0);

        Vector3 dir = Input.mousePosition - Camera.main.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        rb.AddForce(movement * speed);
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

        if (Input.GetMouseButton(0))
        {
            Timer += 1 * Time.deltaTime;

            if (Timer >= ShootDelay)
            {
                playerPos = transform.position;
                playerDirection = transform.right;
                playerRotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z));

                Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

                Rigidbody clone;
                clone = Instantiate(bullet, spawnPos, playerRotation);

                clone.velocity = transform.TransformDirection(Vector3.right * bulletSpeed);

                Timer = 0f;
            }

            //if (Input.GetMouseButtonDown(0))
            //{
            //    playerPos = transform.position;
            //    playerDirection = transform.right;
            //    playerRotation = Quaternion.Euler(new Vector3(transform.rotation.x, transform.rotation.y, transform.rotation.z));

            //    Vector3 spawnPos = playerPos + playerDirection * spawnDistance;

            //    Rigidbody clone;
            //    clone = Instantiate(bullet, spawnPos, playerRotation);

            //    clone.velocity = transform.TransformDirection(Vector3.right * bulletSpeed);
            //}
        }
    }
}
