﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public delegate void HealthChangedAction(float newHealth);

    public event HealthChangedAction onHealthChanged;

    [SerializeField]
    private Image m_HealthBar;

    public float m_MaxHealth = 100f;
    public float m_CurHealth = 0f;

	void Start ()
    {
        m_CurHealth = m_MaxHealth;
	}

    public void ChangeHealth(float Damage)
    {
        m_CurHealth += Damage;
        float CalcHealth = m_CurHealth / m_MaxHealth;

        //DEBUG: print("Health is nu: " + m_CurHealth);
        m_HealthBar.fillAmount = CalcHealth;
        onHealthChanged?.Invoke(m_CurHealth);
    }
}
