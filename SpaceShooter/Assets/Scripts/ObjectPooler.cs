﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }

#region Singleton work around
    public static ObjectPooler Instance;

    private void Awake()
    {
        Instance = this;
    }
#endregion

    public List<Pool> pools;
    public Dictionary<string, Queue<GameObject>> PoolDic;

	void Start ()
    {
        PoolDic = new Dictionary<string, Queue<GameObject>>();

        foreach(Pool pool in pools)
        {
            Queue<GameObject> objectpool = new Queue<GameObject>();

            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                objectpool.Enqueue(obj);
            }

            PoolDic.Add(pool.tag, objectpool);
        }
	}

    public GameObject SpawnFromPool(string tag, Vector3 position, Quaternion rotation)
    {
        GameObject objectspawn = PoolDic[tag].Dequeue();

        objectspawn.SetActive(true);
        objectspawn.transform.position = position;
        objectspawn.transform.rotation = rotation;

        Ipooledobject pooledObj = objectspawn.GetComponent<Ipooledobject>();

        if(pooledObj != null)
        {
            pooledObj.Onobjectspawn();
        }

        PoolDic[tag].Enqueue(objectspawn);

        return objectspawn;
    }
}
