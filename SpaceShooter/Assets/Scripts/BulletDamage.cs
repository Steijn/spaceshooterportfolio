﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDamage : MonoBehaviour
{
    public float m_Damage = 10f;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        AiHealth otherhealth = coll.GetComponent<AiHealth>();

        if (otherhealth != null)
        {
            otherhealth.DecreaseHealths(m_Damage);
            gameObject.SetActive(false);
            Destroy(transform.parent.gameObject);
        }
    }
}
