﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _EnemyController : MonoBehaviour
{
    [SerializeField]
    private Transform M_Player;
    [SerializeField]
    private float M_Speed = 0f;
    [SerializeField]
    private float m_ChaseRange = 0f;

    private void Start()
    {
        M_Player = FindObjectOfType<PlayerController>().transform;
    }
    void Update ()
    {
        float DistanceToTarget = Vector3.Distance(transform.position, M_Player.position);
        if (DistanceToTarget < m_ChaseRange)
        {
            Vector3 Targetdir = M_Player.position - transform.position;
            float angle = Mathf.Atan2(Targetdir.y, Targetdir.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 180);

            transform.Translate(Vector3.right * Time.deltaTime * M_Speed);
        }
    }
}
