﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAI : MonoBehaviour
{
    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.GetComponent<PlayArea>())
        {
            gameObject.SetActive(false);
        }
    }
}
