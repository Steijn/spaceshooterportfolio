﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField]
    private Text m_Score;

    public int m_Number;

	void Update ()
    {
        m_Score.text = m_Number.ToString() + " Score";
    }
}
