﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiSpawner : MonoBehaviour
{
    #region
    //om de enemy te despawnen moet je hem deactivaten met een script

    //public class DestroyEnemy : MonoBehaviour
    //{
    //    private void OnCollisionEnter(Collision collision)
    //    {
    //        gameObject.SetActive(false);
    //    }
    //}
    #endregion

    //maakt de naam van de Objectpooler bruikbaar
    ObjectPooler ObjP;

    //aantal AI die in een keer spawned
    [SerializeField]
    public int m_AantalAI = 0;
    //hoe snel de ai spawned
    [SerializeField]
    public float m_SpawnRate = 0f;
    //de spawnplek van de AI om de spawner heen
    [SerializeField]
    private Vector3 m_Range = new Vector3(0, 0, 0);
    //je mag AI spawnen als de bool true is
    [SerializeField]
    private bool M_MagSpawenen = false;
    //Pool AI Tag
    [SerializeField]
    private string m_AITag;

    //timer die altijd blijft tellen
    private float m_Timer = 0f;

    //maakt een nieuwe vector3 aan voor de gizmos
    private Vector3 m_Spawn = new Vector3(0,0,0);

    private void Start()
    {
        //instansiate objectpooler
        ObjP = ObjectPooler.Instance;
    }

    //OnDrawGizmosSelected tekent de randen van een cube
    private void OnDrawGizmosSelected()
    {
        //geeft de gismos outlines een kleur
        Gizmos.color = Color.blue;
        //geef aan waar hij de gizmos moet tekenen
        Gizmos.DrawWireCube(transform.position, new Vector3(m_Range.x * 2, m_Range.y * 2, m_Range.z * 2));
    }

    void Update()
    {
        //timer blijft tellen todat hij op 0 gezet wordt
        m_Timer += 1 * Time.deltaTime;

        //als de timer gelijk is aan de spawnrate zullen er vijanden spawnen
        if (m_Timer >= m_SpawnRate)
        {
            if (M_MagSpawenen == true)
            {
                //de for runt deze code net zo vaak als m_AantalAI
                for (int i = 0; i < m_AantalAI; i++)
                {
                    //geeft de enemy een random position binnen m_Range
                    m_Spawn.x = Random.Range(transform.position.x - m_Range.x, transform.position.x + m_Range.x);
                    m_Spawn.y = Random.Range(transform.position.y - m_Range.y, transform.position.y + m_Range.y);
                    m_Spawn.z = Random.Range(transform.position.z - m_Range.z, transform.position.z + m_Range.z);

                    //spawn de vijanden van de pool
                    ObjP.SpawnFromPool(m_AITag, m_Spawn, Quaternion.identity);

                    //zet de infinite timer weer op 0
                    m_Timer = 0;
                }
            }
        }
    }
    //TradeMarked by Yari/Steijn
}
